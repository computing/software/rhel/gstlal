%define gstreamername gstreamer1
%global __python %{__python3}

Name: gstlal
Version: 1.13.0
Release: 1.1%{?dist}
Summary: GSTLAL
License: GPL
Group: LSC Software/Data Analysis

# --- package requirements --- #
Requires: avahi
Requires: avahi-glib
Requires: python3-avahi
# avahi-ui-tools depreciated in 0.7-19, remove if not necessary
#Requires: avahi-ui-tools
Requires: fftw >= 3
Requires: gobject-introspection >= 1.30.0
Requires: gsl
Requires: zlib
Requires: orc >= 0.4.16
Requires: %{gstreamername} >= 1.14.1
Requires: %{gstreamername}-plugins-bad-free
Requires: %{gstreamername}-plugins-base >= 1.14.1
Requires: %{gstreamername}-plugins-good >= 1.14.1

# FIXME:  add this when it becomes available, and figure out what its name
# is on .rpm based systems
#Requires: %{gstreamername}-python3-plugin-loader
# FIXME:  add this when it becomes available
#Requires: %{gstreamername}-rtsp-server >= 1.14.1

# --- LSCSoft package requirements --- #
Requires: lal >= 7.2.4
Requires: lalburst >= 1.7.0
Requires: lalmetaio >= 3.0.2
Requires: lalinspiral >= 3.0.2
Requires: lalsimulation >= 4.0.2
Requires: python%{python3_pkgversion}-dqsegdb2 >= 1.0.1
Requires: python%{python3_pkgversion}-gwdatafind >= 1.1.0
Requires: python%{python3_pkgversion}-lal >= 7.2.4
Requires: python%{python3_pkgversion}-ligo-lw >= 1.8.3
Requires: python%{python3_pkgversion}-ligo-segments >= 1.2.0

# --- python package requirements --- #
Requires: python%{python3_pkgversion} >= 3.6
Requires: python%{python3_pkgversion}-dataclasses
Requires: python%{python3_pkgversion}-%{gstreamername}
Requires: python%{python3_pkgversion}-gobject >= 3.22
Requires: python%{python3_pkgversion}-jinja2
Requires: python%{python3_pkgversion}-numpy >= 1.7.0
Requires: python%{python3_pkgversion}-pandas
Requires: python%{python3_pkgversion}-pluggy >= 0.6.0
Requires: python%{python3_pkgversion}-scipy
Requires: python3-condor >= 8.9.0

%if 0%{?rhel} == 8
Requires: python%{python3_pkgversion}-numpy >= 1.7.0
Requires: python3-matplotlib
%else
Requires: numpy >= 1.7.0
Requires: python%{python3_pkgversion}-matplotlib
%endif

# --- build requirements --- #
BuildRequires: doxygen >= 1.8.3
BuildRequires: fftw-devel >= 3
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: graphviz
BuildRequires: gsl-devel
BuildRequires: gtk-doc >= 1.11
BuildRequires: %{gstreamername}-devel >= 1.14.1
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.14.1
# FIXME:  add this when it becomes available
#BuildRequires: %{gstreamername}-rtsp-server-devel >= 1.14.1
BuildRequires: liblal-devel >= 7.2.4
BuildRequires: liblalburst-devel >= 1.7.0
BuildRequires: liblalinspiral-devel >= 3.0.2
BuildRequires: liblalmetaio-devel >= 3.0.2
BuildRequires: liblalsimulation-devel >= 4.0.2
BuildRequires: orc >= 0.4.16
BuildRequires: pkgconfig >= 0.18.0
BuildRequires: python3-devel >= 3.6
# needed for gstpythonplugin.c remove when we remove that plugin from gstlal
BuildRequires: python%{python3_pkgversion}-lal >= 7.2.4
BuildRequires: zlib-devel

# account for package name differences between rhel7/8
%if 0%{?rhel} == 8
BuildRequires: pygobject3-devel >= 3.22
BuildRequires: python%{python3_pkgversion}-numpy >= 1.7.0
%else
BuildRequires: python36-gobject-devel >= 3.22
BuildRequires: numpy >= 1.7.0
%endif

Source: https://software.igwn.org/lscsoft/source/gstlal-%{version}.tar.gz
URL: https://git.ligo.org/lscsoft/gstlal
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: %{name} = %{version}
Requires: fftw-devel >= 3
Requires: gsl-devel
Requires: %{gstreamername}-devel >= 1.14.1
Requires: %{gstreamername}-plugins-base-devel >= 1.14.1
Requires: liblal-devel >= 7.2.4
Requires: liblalmetaio-devel >= 3.0.2
Requires: liblalsimulation-devel >= 4.0.2
Requires: liblalburst-devel >= 1.7.0
Requires: liblalinspiral-devel >= 3.0.2
Requires: python3-devel >= 3.6
%description devel
This package contains the files needed for building gstlal-based plugins
and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure --enable-gtk-doc PYTHON=python3
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gir-*/*
%{_datadir}/gstlal
%{_datadir}/gtk-doc/html/gstlal-*
%{_docdir}/gstlal-*
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/*
%{_libdir}/gstreamer-1.0/*.so
%{_libdir}/gstreamer-1.0/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/gstreamer-1.0/*.a
%{_libdir}/pkgconfig/*
%{_includedir}/*
